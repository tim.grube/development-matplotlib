#!/usr/bin/env python

import matplotlib as mpl
import matplotlib.pyplot as plt
from cycler import cycler

# set global settings
def pre_paper_plot(change=True):
    if not change:
        # Reset back to defaults
        #mpl.rcParams.update(mpl.rcParamsDefault)
        mpl.rcdefaults()
        # Apply own default config (as indicated in the matplotlibrc file)
        params = mpl.rc_params_from_file(mpl.matplotlib_fname())
        mpl.rcParams.update(params)
        return

    plt.rcParams['text.color'] = '000000'
    plt.rcParams['patch.facecolor'] = 'blue'
    plt.rcParams['patch.edgecolor'] = 'black'
    plt.rcParams['axes.facecolor'] = 'white'
    plt.rcParams['axes.edgecolor'] = 'black'
    plt.rcParams['axes.grid'] = False
    plt.rcParams['axes.labelcolor'] = 'black'
    #plt.rcParams['axes.color_cycle'] = '8cd0d3, 7f9f7f, cc9393, 93e0e3, dc8cc3, f0dfaf, dcdccc'
    plt.rcParams['axes.prop_cycle'] = cycler('color', ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf'])
    plt.rcParams['xtick.color'] = 'k'
    plt.rcParams['xtick.direction'] = 'in'
    plt.rcParams['ytick.color'] = 'k'
    plt.rcParams['ytick.direction'] = 'in'
    plt.rcParams['legend.fancybox'] = False
    plt.rcParams['figure.facecolor'] = 'white'
    plt.rcParams['figure.edgecolor'] = 'white'
    plt.rcParams['text.usetex'] = True

    plt.rcParams['figure.figsize'] = (8, 3)
    plt.rcParams['font.size'] = 12
    plt.rcParams['font.family'] = 'Computer Modern'
    plt.rcParams['axes.labelsize'] = 14
    plt.rcParams['axes.titlesize'] = 14
    plt.rcParams['legend.fontsize'] = 10
    plt.rcParams['xtick.labelsize'] = plt.rcParams['font.size']
    plt.rcParams['ytick.labelsize'] = plt.rcParams['font.size']
    plt.rcParams['savefig.dpi'] = 300
    plt.rcParams['xtick.major.size'] = 3
    plt.rcParams['xtick.minor.size'] = 3
    plt.rcParams['xtick.major.width'] = 1
    plt.rcParams['xtick.minor.width'] = 1
    plt.rcParams['ytick.major.size'] = 3
    plt.rcParams['ytick.minor.size'] = 3
    plt.rcParams['ytick.major.width'] = 1
    plt.rcParams['ytick.minor.width'] = 1
    plt.rcParams['legend.frameon'] = True
    plt.rcParams['legend.edgecolor'] = 'k'
    plt.rcParams['legend.loc'] = 'best'
    plt.rcParams['axes.linewidth'] = 1

def post_paper_plot(change=True, bw_friendly=False, adjust_spines=False):
    if not change:
        return
    if adjust_spines:
        plt.gca().spines['right'].set_color('none')
        plt.gca().spines['top'].set_color('none')
    plt.gca().xaxis.set_ticks_position('bottom')
    plt.gca().yaxis.set_ticks_position('left')
    if bw_friendly:
        setFigLinesBW(plt.gcf())

# Following functions taken from:
# https://stackoverflow.com/questions/7358118/matplotlib-black-white-colormap-with-dashes-dots-etc

def setAxLinesBW(ax):
    """
    Take each Line2D in the axes, ax, and convert the line style to be 
    suitable for black and white viewing.
    """
    marker_size = 5

    color_map = {
        '#1f77b4': {'marker': None, 'dash': (None,None)},
        '#ff7f0e': {'marker': None, 'dash': [3,4]},
        '#2ca02c': {'marker': None, 'dash': [5,3,2,1]},
        '#d62728': {'marker': None, 'dash': [1,3]},
        '#9467bd': {'marker': None, 'dash': [5,2,5,2,5,10]},
        '#8c564b': {'marker': None, 'dash': [5,3,1,2,1,10]},
        '#e377c2': {'marker': 'o', 'dash': (None,None)} #[1,2,1,10]}
        }


    lines_to_adjust = ax.get_lines()
    try:
        lines_to_adjust += ax.get_legend().get_lines()
    except AttributeError:
        pass

    for line in lines_to_adjust:
        orig_color = line.get_color()
        #line.set_color('black')
        line.set_dashes(color_map[orig_color]['dash'])
        line.set_marker(color_map[orig_color]['marker'])
        line.set_markersize(marker_size)

def setFigLinesBW(fig):
    """
    Take each axes in the figure, and for each line in the axes, make the
    line viewable in black and white.
    """
    for ax in fig.get_axes():
        setAxLinesBW(ax)
